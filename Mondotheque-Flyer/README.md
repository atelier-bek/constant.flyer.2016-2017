Quick start
===========

Pre-requisites
--------------

Clone and enter the repository

    git clone git@git.constantvzw.org:medor.numero2.git
    cd medor.numero2

Make sure you have all the system-wide dependencies

    sudo apt-get update
    sudo apt-get install python-dev libxml-dev libxslt-dev
    sudo apt-get install libffi-dev
    sudo apt-get install python-virtualenv
    sudo apt-get install npm
    sudo apt-get install nodejs-legacy
    sudo npm -global install bower


Install the js and css dependencies (it will create a "vendors" directory)

    bower install

Install the python dependencies

    virtualenv --no-site-packages venv
    source venv/bin/activate
    pip install -r requirements.txt

Copy the exemple settings file and set the credentials for medor.coop. They will
be used to fetch the articles from the plateform

    cp bin/settings.example.py bin/settings.py
    nano bin/settings.py

Pull the stories in the stories directory

    make stories

Generate the table of contents for HTML2print to work

    make build/js/src.js


Using HTML2print
----------------

Starts the serveur

    python2 -m SimpleHTTPServer

Alternatively, There is a little script based on SimpleHTTPServer that forces
the browser not to cache the files

    python2 bin/serveit.py

Visit the URL:

    http://localhost:8000

keeping everything up-to-date
=============================

Updating the stories
--------------------

Enter the virtualenv if it is not yet the case

    source venv/bin/activate

Pull the stories in the stories directory

    make stories

Updating the table of contents
------------------------------

Enter the virtualenv if it is not yet the case

    source venv/bin/activate

Generate the table of contents for HTML2print to work

    make js/src.js


Generating CMYK PDFs for print
------------------------------

Put your RGB PDFs in `_batRGB` inside each `cahier#` folder.

    make cmyk

It will produce:
    - `_batCMYK` folder with corresponding `cahier#` folders
    - `_bat-plates` folder with html pages to preview CMYK plates


Generating Cahier PDFs for print
--------------------------------

    make cahiers

It will produce a folder `_batCAHIERS` with these PDFs (according to the structure of `_batCMYK` folder:
    - cahier1.pdf
    - cahier2.pdf
    - cahier3.pdf
    - cahier4.pdf


Cookbook
========

Convertir les textes pour un premier coulage (avant l'étape de corrections)
--------------------------------------------------------------------------

1_ convertir le docx en html
pandoc -f docx -t html origin.docx > result.html
2_ convertir cet html en markdown
html2text article.html > article.md 
3_convertir le markdown en html
markdown_py -x extra article.md > article.html
4_ importer cet html dans l'administration Django


Converting from HTML to .docx
-----------------------------

    pandoc -f html -t docx -o destination.docx source.html

Adapt it to your needs if you need to work with other formats
